# Ansible role - Open vSwitch

This role serves for automatic installation of multilayer virtual switch Open vSwitch on Debian-based OS.

## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Role Parameters

* `openvswitch_version` (optional) - Version of Open vSwitch to install. If specified, role will install particular version of Open vSwitch from source, otherwise latest version from OS repository.

## Example 

Example of simple PostgreSQL installation.

```yml
roles:
    - role: openvswitch
      openvswitch_version: 2.8.1
      become: yes
```